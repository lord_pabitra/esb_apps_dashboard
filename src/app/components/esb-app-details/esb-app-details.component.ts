import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-esb-app-details',
  templateUrl: './esb-app-details.component.html',
  styleUrls: ['./esb-app-details.component.scss']
})
export class EsbAppDetailsComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit(): void {
  }

  goToDashboardPage(event) {
    event.preventDefault();
    this._router.navigate(['']);
  }

}
