import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsbAppDetailsComponent } from './esb-app-details.component';

describe('EsbAppDetailsComponent', () => {
  let component: EsbAppDetailsComponent;
  let fixture: ComponentFixture<EsbAppDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsbAppDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsbAppDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
