import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverallDownCountComponent } from './overall-down-count.component';

describe('OverallDownCountComponent', () => {
  let component: OverallDownCountComponent;
  let fixture: ComponentFixture<OverallDownCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverallDownCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverallDownCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
