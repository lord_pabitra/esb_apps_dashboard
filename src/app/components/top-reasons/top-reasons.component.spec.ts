import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopReasonsComponent } from './top-reasons.component';

describe('TopReasonsComponent', () => {
  let component: TopReasonsComponent;
  let fixture: ComponentFixture<TopReasonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopReasonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopReasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
