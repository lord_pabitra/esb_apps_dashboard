import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvWiseDowmCountComponent } from './env-wise-dowm-count.component';

describe('EnvWiseDowmCountComponent', () => {
  let component: EnvWiseDowmCountComponent;
  let fixture: ComponentFixture<EnvWiseDowmCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvWiseDowmCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvWiseDowmCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
