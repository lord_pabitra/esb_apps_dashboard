import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { Label, BaseChartDirective } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

import { EnvWiseDowmCount } from '../../models/env-wise-dowm-count.model';
import { DeviceWidth } from '../../models/device-width.model';
import { DashboardService} from '../../services/dashboard.service';

import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-env-wise-dowm-count',
  templateUrl: './env-wise-dowm-count.component.html',
  styleUrls: ['./env-wise-dowm-count.component.scss']
})
export class EnvWiseDowmCountComponent implements OnInit, OnDestroy {
  public selectedView: string = 'daily';
  public pieChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['Dev', 'QA','UAT', 'Prod'];
  public pieChartData: number[] = [1,2,3,4];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)', '#00aeef'],
    },
  ];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  private _deviceWidthSubscription: Subscription;
  public pieChartContainerCls: string = '';

  constructor(private _dashboardService: DashboardService) { 
  }

  ngOnInit() {
    this.setDayWiseDownCount();
    this._deviceWidthSubscription = this._dashboardService.deviceWidth$.subscribe( (deviceWidth: DeviceWidth) => {
      this.resizeChart(deviceWidth);
    });
  }

  ngOnDestroy(): void {
    this._deviceWidthSubscription ? this._deviceWidthSubscription.unsubscribe() : null;
  }
  
  private resizeChart(deviceWidth: DeviceWidth) {
    if(deviceWidth === DeviceWidth.XS){
      this.pieChartOptions.maintainAspectRatio = false;
      this.pieChartContainerCls = 'pie-chart-container pie-chart-container-mob';
    }
    else {
      this.pieChartOptions.maintainAspectRatio = true;
      this.pieChartContainerCls = 'pie-chart-container';
    }
    this.chart.update();
  }

  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  changeLegendPosition() {
    this.pieChartOptions.legend.position = this.pieChartOptions.legend.position === 'left' ? 'top' : 'left';
  }

  setDayWiseDownCount(){
    this.setPieChartData(
      Object.keys(OVERALL_DWON_COUNT.dayWise).map((key) => parseInt(OVERALL_DWON_COUNT.dayWise[key]))
    );
  }

  setWeekWiseDownCount(){
    this.setPieChartData(
      Object.keys(OVERALL_DWON_COUNT.weekWise).map((key) => parseInt(OVERALL_DWON_COUNT.weekWise[key]))
    );
  }

  setMonthWiseDownCount(){
    this.setPieChartData(
      Object.keys(OVERALL_DWON_COUNT.monthWise).map((key) => parseInt(OVERALL_DWON_COUNT.monthWise[key]))
    );
  }

  private setPieChartData(chartData: number[]){
    this.pieChartData = chartData;
    this.chart.update();
  }

  onSelectedViewChange(viewChangedEvent: MatButtonToggleChange) {
    if(this.selectedView !== viewChangedEvent.value){
      this.selectedView = viewChangedEvent.value;
      switch(this.selectedView) {
        case 'daily':
          this.setDayWiseDownCount();
          break;
        case 'weekly':
          this.setWeekWiseDownCount();
          break;
        case 'monthly':
          this.setMonthWiseDownCount();
          break;
        default:
          break;
      }
    }
  }

}


const OVERALL_DWON_COUNT: EnvWiseDowmCount = {
  dayWise: {
    dev: 1,
    qa: 2,
    uat: 3,
    prod: 4
  },
  weekWise: {
    dev: 10,
    qa: 15,
    uat: 2,
    prod: 4
  },
  monthWise: {
    dev: 40,
    qa: 23,
    uat: 5,
    prod: 10
  }
}