import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { EsbAppDetailsComponent } from '../esb-app-details/esb-app-details.component';


export interface EsbApp {
  id: number;
  name: string;
  status: string;
  env: string,
  detailsLink: string;
  description?: string
}

@Component({
  selector: 'app-esb-app-list',
  templateUrl: './esb-app-list.component.html',
  styleUrls: ['./esb-app-list.component.scss']
})

export class EsbAppListComponent implements OnInit {
  displayedColumns: string[] = ['name','env','status', 'id'];
  dataSource: MatTableDataSource<EsbApp>;
  @Output() detailsLinkClicked = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor() {
    // Create 100 users
    const esbApps = Array.from({length: 100}, (_, k) => createNewEsbApp(k + 1));

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(esbApps);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  goToDetailsPage(event: any, id: number): void{
    event.preventDefault();
    this.detailsLinkClicked.emit(id);
  }

  getStyleClassforStatusColumn(status: string): string {
    switch(status){
      case 'Up':
        return 'status status-green';
      case 'Down':
        return 'status status-red';
      case 'NA':
        return 'status status-blue';
      default:
        return 'status status-blue';
    }
  }
}

function createNewEsbApp(id: number): EsbApp {
  const ESB_SAMPLE_APP_NAMES = ['PAPI_TRAVEL_GOTREX','PAPI_ADDRESS_VALIDATION','PAPI_DRIVER_INFO','PAPI_AIR_MILES','PAPI_FULFILLMENT_GOTREX','PAPI_POLICY_SEARCH','PAPI_POLICY_RETRIEVE','XAPI_GSL','PAPI_MVR','PAPI_MAIL_SENDER'];
  const status: string = id%3 === 0 ? 'NA' : (id%2 === 0 ? 'Down' : 'Up');
  const env: string = id%3 === 0 ? 'Dev' : (id%2 === 0 ? 'QA' : 'Production');
  return {
    id: id,
    name: ESB_SAMPLE_APP_NAMES[id%10],
    env: env,
    status: status,
    detailsLink: 'details/id',
  }
}