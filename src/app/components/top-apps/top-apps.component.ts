import { Component, OnInit } from '@angular/core';
import { TopApp } from '../../models/top-app.model';

@Component({
  selector: 'app-top-apps',
  templateUrl: './top-apps.component.html',
  styleUrls: ['./top-apps.component.scss']
})
export class TopAppsComponent implements OnInit {

  topApps: TopApp[];

  constructor() { 
    this.topApps = TOP_APPS;
  }

  ngOnInit(): void {
  }

}

const TOP_APPS: TopApp[] = [
  {name: 'PAPI_TRAVEL_GOTREX',environment: 'QA',downCount: 16},
  {name: 'PAPI_ADDRESS_VALIDATION',environment: 'Development',downCount: 10},
  {name: 'PAPI_FULFILLMENT_GOTREX',environment: 'UAT',downCount: 10},
  {name: 'PAPI_MAIL_SENDER',environment: 'Development',downCount: 7},
  {name: 'PAPI_AIR_MILES',environment: 'Production',downCount: 5}
];