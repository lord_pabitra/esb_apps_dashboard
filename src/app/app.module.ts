import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {CdkTableModule} from '@angular/cdk/table';
import { AngularMaterialModule } from './angular-material/angular-material.module';

import { NgxUiLoaderModule, NgxUiLoaderHttpModule, NgxUiLoaderRouterModule  } from 'ngx-ui-loader';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EsbAppDetailsComponent } from './components/esb-app-details/esb-app-details.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { PageFooterComponent } from './components/page-footer/page-footer.component';
import { EsbAppListComponent } from './components/esb-app-list/esb-app-list.component';
import { TopReasonsComponent } from './components/top-reasons/top-reasons.component';
import { TopAppsComponent } from './components/top-apps/top-apps.component';
import { OverallDownCountComponent } from './components/overall-down-count/overall-down-count.component';
import { EnvWiseDowmCountComponent } from './components/env-wise-dowm-count/env-wise-dowm-count.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    EsbAppDetailsComponent,
    PageHeaderComponent,
    PageFooterComponent,
    EsbAppListComponent,
    TopReasonsComponent,
    TopAppsComponent,
    OverallDownCountComponent,
    EnvWiseDowmCountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ChartsModule,
    NgxUiLoaderModule, // import NgxUiLoaderModule
    NgxUiLoaderRouterModule ,
    NgxUiLoaderHttpModule.forRoot({ showForeground: true }),
    CdkTableModule,
    AngularMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
