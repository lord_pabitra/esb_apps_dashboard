export class TopReason {
    public heading: string;
    public details: string;
    public occurance: number;
}
